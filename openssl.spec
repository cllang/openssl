# For the curious:
# 0.9.8jk + EAP-FAST soversion = 8
# 1.0.0 soversion = 10
# 1.1.0 soversion = 1.1 (same as upstream although presence of some symbols
#                        depends on build configuration options)
# 3.0.0 soversion = 3 (same as upstream)
%define soversion 3

# Arches on which we need to prevent arch conflicts on opensslconf.h, must
# also be handled in opensslconf-new.h.
%define multilib_arches %{ix86} ia64 %{mips} ppc ppc64 s390 s390x sparcv9 sparc64 x86_64

%define srpmhash() %{lua:
local files = rpm.expand("%_specdir/openssl.spec")
for i, p in ipairs(patches) do
   files = files.." "..p
end
for i, p in ipairs(sources) do
   files = files.." "..p
end
local sha256sum = assert(io.popen("cat "..files.." 2>/dev/null | sha256sum"))
local hash = sha256sum:read("*a")
sha256sum:close()
print(string.sub(hash, 0, 16))
}

%global _performance_build 1

Summary: Utilities from the general purpose cryptography library with TLS implementation
Name: openssl
Version: 3.0.1
Release: 35%{?dist}
Epoch: 1
# We have to remove certain patented algorithms from the openssl source
# tarball with the hobble-openssl script which is included below.
# The original openssl upstream tarball cannot be shipped in the .src.rpm.
Source: openssl-%{version}-hobbled.tar.xz
Source1: hobble-openssl
Source2: Makefile.certificate
Source3: genpatches
Source6: make-dummy-cert
Source7: renew-dummy-cert
Source9: configuration-switch.h
Source10: configuration-prefix.h
Source12: ec_curve.c
Source13: ectest.c
Source14: 0025-for-tests.patch

# Patches exported from source git
# Aarch64 and ppc64le use lib64
Patch1: 0001-Aarch64-and-ppc64le-use-lib64.patch
# Use more general default values in openssl.cnf
Patch2: 0002-Use-more-general-default-values-in-openssl.cnf.patch
# Do not install html docs
Patch3: 0003-Do-not-install-html-docs.patch
# Override default paths for the CA directory tree
Patch4: 0004-Override-default-paths-for-the-CA-directory-tree.patch
# apps/ca: fix md option help text
Patch5: 0005-apps-ca-fix-md-option-help-text.patch
# Disable signature verification with totally unsafe hash algorithms
Patch6: 0006-Disable-signature-verification-with-totally-unsafe-h.patch
# Add support for PROFILE=SYSTEM system default cipherlist
Patch7: 0007-Add-support-for-PROFILE-SYSTEM-system-default-cipher.patch
# Add FIPS_mode() compatibility macro
Patch8: 0008-Add-FIPS_mode-compatibility-macro.patch
# Add check to see if fips flag is enabled in kernel
Patch9: 0009-Add-Kernel-FIPS-mode-flag-support.patch
# remove unsupported EC curves
Patch11: 0011-Remove-EC-curves.patch
# Disable explicit EC curves
# https://bugzilla.redhat.com/show_bug.cgi?id=2066412
Patch12: 0012-Disable-explicit-ec.patch
# https://github.com/openssl/openssl/pull/17981
Patch13: 0013-FIPS-provider-explicit-ec.patch
# https://github.com/openssl/openssl/pull/17998
Patch14: 0014-FIPS-disable-explicit-ec.patch
# Instructions to load legacy provider in openssl.cnf
Patch24: 0024-load-legacy-prov.patch
# Tmp: test name change
Patch31: 0031-tmp-Fix-test-names.patch
# We load FIPS provider and set FIPS properties implicitly
Patch32: 0032-Force-fips.patch
# Embed HMAC into the fips.so
Patch33: 0033-FIPS-embed-hmac.patch
# Comment out fipsinstall command-line utility
Patch34: 0034.fipsinstall_disable.patch
# Skip unavailable algorithms running `openssl speed`
Patch35: 0035-speed-skip-unavailable-dgst.patch
# Minimize fips services
Patch45: 0045-FIPS-services-minimize.patch
# Backport of s390x hardening, https://github.com/openssl/openssl/pull/17486
Patch46: 0046-FIPS-s390x-hardening.patch
# Execute KATS before HMAC verification
Patch47: 0047-FIPS-early-KATS.patch
# Backport of correctly handle 2^14 byte long records #17538
Patch48: 0048-correctly-handle-records.patch
# Selectively disallow SHA1 signatures
Patch49: 0049-Selectively-disallow-SHA1-signatures.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=2049265
Patch50: 0050-FIPS-enable-pkcs12-mac.patch
# Backport of patch for RHEL for Edge rhbz #2027261
Patch51: 0051-Support-different-R_BITS-lengths-for-KBKDF.patch
# Allow SHA1 in seclevel 2 if rh-allow-sha1-signatures = yes
Patch52: 0052-Allow-SHA1-in-seclevel-2-if-rh-allow-sha1-signatures.patch
# CVE 2022-0778
Patch53: 0053-CVE-2022-0778.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=2004915, backport of 2c0f7d46b8449423446cfe1e52fc1e1ecd506b62
Patch54: 0054-Replace-size-check-with-more-meaningful-pubkey-check.patch
# https://github.com/openssl/openssl/pull/17324
Patch55: 0055-nonlegacy-fetch-null-deref.patch
# https://github.com/openssl/openssl/pull/18103
Patch56: 0056-strcasecmp.patch
# https://github.com/openssl/openssl/pull/18175
Patch57: 0057-strcasecmp-fix.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=2053289
Patch58: 0058-FIPS-limit-rsa-encrypt.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=2069235
Patch60: 0060-FIPS-KAT-signature-tests.patch
# https://bugzilla.redhat.com/show_bug.cgi?id=2087147
Patch61: 0061-Deny-SHA-1-signature-verification-in-FIPS-provider.patch
Patch62: 0062-fips-Expose-a-FIPS-indicator.patch
# https://github.com/openssl/openssl/pull/18141
Patch63: 0063-CVE-2022-1473.patch
# upstream commits 55c80c222293a972587004c185dc5653ae207a0e 2eda98790c5c2741d76d23cc1e74b0dc4f4b391a
Patch64: 0064-CVE-2022-1343.diff
# upstream commit 1ad73b4d27bd8c1b369a3cd453681d3a4f1bb9b2
Patch65: 0065-CVE-2022-1292.patch
# https://github.com/openssl/openssl/pull/18444
# https://github.com/openssl/openssl/pull/18467
Patch66: 0066-replace-expired-certs.patch

License: ASL 2.0
URL: http://www.openssl.org/
BuildRequires: gcc g++
BuildRequires: coreutils, perl-interpreter, sed, zlib-devel, /usr/bin/cmp
BuildRequires: lksctp-tools-devel
BuildRequires: /usr/bin/rename
BuildRequires: /usr/bin/pod2man
BuildRequires: /usr/sbin/sysctl
BuildRequires: perl(Test::Harness), perl(Test::More), perl(Math::BigInt)
BuildRequires: perl(Module::Load::Conditional), perl(File::Temp)
BuildRequires: perl(Time::HiRes), perl(IPC::Cmd), perl(Pod::Html), perl(Digest::SHA)
BuildRequires: perl(FindBin), perl(lib), perl(File::Compare), perl(File::Copy), perl(bigint)
BuildRequires: git-core
Requires: coreutils
Requires: %{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}

%description
The OpenSSL toolkit provides support for secure communications between
machines. OpenSSL includes a certificate management tool and shared
libraries which provide various cryptographic algorithms and
protocols.

%package libs
Summary: A general purpose cryptography library with TLS implementation
Requires: ca-certificates >= 2008-5
Requires: crypto-policies >= 20180730
Recommends: openssl-pkcs11%{?_isa}

%description libs
OpenSSL is a toolkit for supporting cryptography. The openssl-libs
package contains the libraries that are used by various applications which
support cryptographic algorithms and protocols.

%package devel
Summary: Files for development of applications which will use OpenSSL
Requires: %{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}
Requires: pkgconfig

%description devel
OpenSSL is a toolkit for supporting cryptography. The openssl-devel
package contains include files needed to develop applications which
support various cryptographic algorithms and protocols.

%package perl
Summary: Perl scripts provided with OpenSSL
Requires: perl-interpreter
Requires: %{name}%{?_isa} = %{epoch}:%{version}-%{release}

%description perl
OpenSSL is a toolkit for supporting cryptography. The openssl-perl
package provides Perl scripts for converting certificates and keys
from other formats to the formats used by the OpenSSL toolkit.

%prep
%autosetup -S git -n %{name}-%{version}

# The hobble_openssl is called here redundantly, just to be sure.
# The tarball has already the sources removed.
%{SOURCE1} > /dev/null

cp %{SOURCE12} crypto/ec/
cp %{SOURCE13} test/

%build
# Figure out which flags we want to use.
# default
sslarch=%{_os}-%{_target_cpu}
%ifarch %ix86
sslarch=linux-elf
if ! echo %{_target} | grep -q i686 ; then
	sslflags="no-asm 386"
fi
%endif
%ifarch x86_64
sslflags=enable-ec_nistp_64_gcc_128
%endif
%ifarch sparcv9
sslarch=linux-sparcv9
sslflags=no-asm
%endif
%ifarch sparc64
sslarch=linux64-sparcv9
sslflags=no-asm
%endif
%ifarch alpha alphaev56 alphaev6 alphaev67
sslarch=linux-alpha-gcc
%endif
%ifarch s390 sh3eb sh4eb
sslarch="linux-generic32 -DB_ENDIAN"
%endif
%ifarch s390x
sslarch="linux64-s390x"
%endif
%ifarch %{arm}
sslarch=linux-armv4
%endif
%ifarch aarch64
sslarch=linux-aarch64
sslflags=enable-ec_nistp_64_gcc_128
%endif
%ifarch sh3 sh4
sslarch=linux-generic32
%endif
%ifarch ppc64 ppc64p7
sslarch=linux-ppc64
%endif
%ifarch ppc64le
sslarch="linux-ppc64le"
sslflags=enable-ec_nistp_64_gcc_128
%endif
%ifarch mips mipsel
sslarch="linux-mips32 -mips32r2"
%endif
%ifarch mips64 mips64el
sslarch="linux64-mips64 -mips64r2"
%endif
%ifarch mips64el
sslflags=enable-ec_nistp_64_gcc_128
%endif
%ifarch riscv64
sslarch=linux-generic64
%endif

# Add -Wa,--noexecstack here so that libcrypto's assembler modules will be
# marked as not requiring an executable stack.
# Also add -DPURIFY to make using valgrind with openssl easier as we do not
# want to depend on the uninitialized memory as a source of entropy anyway.
RPM_OPT_FLAGS="$RPM_OPT_FLAGS -Wa,--noexecstack -Wa,--generate-missing-build-notes=yes -DPURIFY $RPM_LD_FLAGS"

export HASHBANGPERL=/usr/bin/perl

%define fips %{version}-%{srpmhash}
# ia64, x86_64, ppc are OK by default
# Configure the build tree.  Override OpenSSL defaults with known-good defaults
# usable on all platforms.  The Configure script already knows to use -fPIC and
# RPM_OPT_FLAGS, so we can skip specifiying them here.
./Configure \
	--prefix=%{_prefix} --openssldir=%{_sysconfdir}/pki/tls ${sslflags} \
	--system-ciphers-file=%{_sysconfdir}/crypto-policies/back-ends/openssl.config \
	zlib enable-camellia enable-seed enable-rfc3779 enable-sctp \
	enable-cms enable-md2 enable-rc5 enable-ktls enable-fips\
	no-mdc2 no-ec2m no-sm2 no-sm4 enable-buildtest-c++\
	shared  ${sslarch} $RPM_OPT_FLAGS '-DDEVRANDOM="\"/dev/urandom\"" -DREDHAT_FIPS_VERSION="\"%{fips}\""'

# Do not run this in a production package the FIPS symbols must be patched-in
#util/mkdef.pl crypto update

make %{?_smp_mflags} all

# Clean up the .pc files
for i in libcrypto.pc libssl.pc openssl.pc ; do
  sed -i '/^Libs.private:/{s/-L[^ ]* //;s/-Wl[^ ]* //}' $i
done

%check
# Verify that what was compiled actually works.

# Hack - either enable SCTP AUTH chunks in kernel or disable sctp for check
(sysctl net.sctp.addip_enable=1 && sysctl net.sctp.auth_enable=1) || \
(echo 'Failed to enable SCTP AUTH chunks, disabling SCTP for tests...' &&
 sed '/"msan" => "default",/a\ \ "sctp" => "default",' configdata.pm > configdata.pm.new && \
 touch -r configdata.pm configdata.pm.new && \
 mv -f configdata.pm.new configdata.pm)

# We must revert patch4 before tests otherwise they will fail
patch -p1 -R < %{PATCH4}
#We must disable default provider before tests otherwise they will fail
patch -p1 < %{SOURCE14}

OPENSSL_ENABLE_MD5_VERIFY=
export OPENSSL_ENABLE_MD5_VERIFY
OPENSSL_ENABLE_SHA1_SIGNATURES=
export OPENSSL_ENABLE_SHA1_SIGNATURES
OPENSSL_SYSTEM_CIPHERS_OVERRIDE=xyz_nonexistent_file
export OPENSSL_SYSTEM_CIPHERS_OVERRIDE
#embed HMAC into fips provider for test run
LD_LIBRARY_PATH=. apps/openssl dgst -binary -sha256 -mac HMAC -macopt hexkey:f4556650ac31d35461610bac4ed81b1a181b2d8a43ea2854cbae22ca74560813 < providers/fips.so > providers/fips.so.hmac
objcopy --update-section .rodata1=providers/fips.so.hmac providers/fips.so providers/fips.so.mac
mv providers/fips.so.mac providers/fips.so
#run tests itself
make test HARNESS_JOBS=8

# Add generation of HMAC checksum of the final stripped library
# We manually copy standard definition of __spec_install_post
# and add hmac calculation/embedding to fips.so
%define __spec_install_post \
    %{?__debug_package:%{__debug_install_post}} \
    %{__arch_install_post} \
    %{__os_install_post} \
    LD_LIBRARY_PATH=. apps/openssl dgst -binary -sha256 -mac HMAC -macopt hexkey:f4556650ac31d35461610bac4ed81b1a181b2d8a43ea2854cbae22ca74560813 < $RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so > $RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so.hmac \
    objcopy --update-section .rodata1=$RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so.hmac $RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so $RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so.mac \
    mv $RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so.mac $RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so \
    rm $RPM_BUILD_ROOT%{_libdir}/ossl-modules/fips.so.hmac \
%{nil}

%define __provides_exclude_from %{_libdir}/openssl

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
# Install OpenSSL.
install -d $RPM_BUILD_ROOT{%{_bindir},%{_includedir},%{_libdir},%{_mandir},%{_libdir}/openssl,%{_pkgdocdir}}
%make_install
rename so.%{soversion} so.%{version} $RPM_BUILD_ROOT%{_libdir}/*.so.%{soversion}
for lib in $RPM_BUILD_ROOT%{_libdir}/*.so.%{version} ; do
	chmod 755 ${lib}
	ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`
	ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`.%{soversion}
done

# Remove static libraries
for lib in $RPM_BUILD_ROOT%{_libdir}/*.a ; do
	rm -f ${lib}
done

# Install a makefile for generating keys and self-signed certs, and a script
# for generating them on the fly.
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/certs
install -m644 %{SOURCE2} $RPM_BUILD_ROOT%{_pkgdocdir}/Makefile.certificate
install -m755 %{SOURCE6} $RPM_BUILD_ROOT%{_bindir}/make-dummy-cert
install -m755 %{SOURCE7} $RPM_BUILD_ROOT%{_bindir}/renew-dummy-cert

# Move runable perl scripts to bindir
mv $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/misc/*.pl $RPM_BUILD_ROOT%{_bindir}
mv $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/misc/tsget $RPM_BUILD_ROOT%{_bindir}

# Rename man pages so that they don't conflict with other system man pages.
pushd $RPM_BUILD_ROOT%{_mandir}
mv man5/config.5ossl man5/openssl.cnf.5
popd

mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA
mkdir -m700 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/private
mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/certs
mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/crl
mkdir -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/newcerts

# Ensure the config file timestamps are identical across builds to avoid
# mulitlib conflicts and unnecessary renames on upgrade
touch -r %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/openssl.cnf
touch -r %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/ct_log_list.cnf

rm -f $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/openssl.cnf.dist
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/ct_log_list.cnf.dist
#we don't use native fipsmodule.cnf because FIPS module is loaded automatically
rm -f $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/fipsmodule.cnf

# Determine which arch opensslconf.h is going to try to #include.
basearch=%{_arch}
%ifarch %{ix86}
basearch=i386
%endif
%ifarch sparcv9
basearch=sparc
%endif
%ifarch sparc64
basearch=sparc64
%endif

# Next step of gradual disablement of SSL3.
# Make SSL3 disappear to newly built dependencies.
sed -i '/^\#ifndef OPENSSL_NO_SSL_TRACE/i\
#ifndef OPENSSL_NO_SSL3\
# define OPENSSL_NO_SSL3\
#endif' $RPM_BUILD_ROOT/%{_prefix}/include/openssl/opensslconf.h

%ifarch %{multilib_arches}
# Do an configuration.h switcheroo to avoid file conflicts on systems where you
# can have both a 32- and 64-bit version of the library, and they each need
# their own correct-but-different versions of opensslconf.h to be usable.
install -m644 %{SOURCE10} \
	$RPM_BUILD_ROOT/%{_prefix}/include/openssl/configuration-${basearch}.h
cat $RPM_BUILD_ROOT/%{_prefix}/include/openssl/configuration.h >> \
	$RPM_BUILD_ROOT/%{_prefix}/include/openssl/configuration-${basearch}.h
install -m644 %{SOURCE9} \
	$RPM_BUILD_ROOT/%{_prefix}/include/openssl/configuration.h
%endif

%files
%{!?_licensedir:%global license %%doc}
%license LICENSE.txt
%doc NEWS.md README.md
%{_bindir}/make-dummy-cert
%{_bindir}/renew-dummy-cert
%{_bindir}/openssl
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man7/*
%{_pkgdocdir}/Makefile.certificate
%exclude %{_mandir}/man1/*.pl*
%exclude %{_mandir}/man1/tsget*

%files libs
%{!?_licensedir:%global license %%doc}
%license LICENSE.txt
%dir %{_sysconfdir}/pki/tls
%dir %{_sysconfdir}/pki/tls/certs
%dir %{_sysconfdir}/pki/tls/misc
%dir %{_sysconfdir}/pki/tls/private
%config(noreplace) %{_sysconfdir}/pki/tls/openssl.cnf
%config(noreplace) %{_sysconfdir}/pki/tls/ct_log_list.cnf
%attr(0755,root,root) %{_libdir}/libcrypto.so.%{version}
%{_libdir}/libcrypto.so.%{soversion}
%attr(0755,root,root) %{_libdir}/libssl.so.%{version}
%{_libdir}/libssl.so.%{soversion}
%attr(0755,root,root) %{_libdir}/engines-%{soversion}
%attr(0755,root,root) %{_libdir}/ossl-modules

%files devel
%doc CHANGES.md doc/dir-locals.example.el doc/openssl-c-indent.el
%{_prefix}/include/openssl
%{_libdir}/*.so
%{_mandir}/man3/*
%{_libdir}/pkgconfig/*.pc

%files perl
%{_bindir}/c_rehash
%{_bindir}/*.pl
%{_bindir}/tsget
%{_mandir}/man1/*.pl*
%{_mandir}/man1/tsget*
%dir %{_sysconfdir}/pki/CA
%dir %{_sysconfdir}/pki/CA/private
%dir %{_sysconfdir}/pki/CA/certs
%dir %{_sysconfdir}/pki/CA/crl
%dir %{_sysconfdir}/pki/CA/newcerts

%ldconfig_scriptlets libs

%changelog
* Wed Jun 08 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-35
- Add explicit indicators for signatures in FIPS mode and mark signature
  primitives as unapproved.
  Resolves: rhbz#2087147

* Fri Jun 03 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-34
- Some OpenSSL test certificates are expired, updating
- Resolves: rhbz#2092456

* Thu May 26 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-33
- CVE-2022-1473 openssl: OPENSSL_LH_flush() breaks reuse of memory
- Resolves: rhbz#2089444
- CVE-2022-1343 openssl: Signer certificate verification returned
  inaccurate response when using OCSP_NOCHECKS
- Resolves: rhbz#2087911
- CVE-2022-1292 openssl: c_rehash script allows command injection
- Resolves: rhbz#2090362
- Revert "Disable EVP_PKEY_sign/EVP_PKEY_verify in FIPS mode"
  Related: rhbz#2087147
- Use KAT for ECDSA signature tests, s390 arch
- Resolves: rhbz#2069235

* Thu May 19 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-32
- `openssl ecparam -list_curves` lists only FIPS-approved curves in FIPS mode
- Resolves: rhbz#2083240
- Ciphersuites with RSA KX should be filterd in FIPS mode
- Related: rhbz#2085088
- In FIPS mode, signature verification works with keys of arbitrary size
  above 2048 bit, and only with 1024, 1280, 1536, 1792 bits for keys
  below 2048 bits
- Resolves: rhbz#2077884

* Wed May 18 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-31
- Disable SHA-1 signature verification in FIPS mode
- Disable EVP_PKEY_sign/EVP_PKEY_verify in FIPS mode
  Resolves: rhbz#2087147

* Mon May 16 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-30
- Use KAT for ECDSA signature tests
- Resolves: rhbz#2069235

* Thu May 12 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-29
- `-config` argument of openssl app should work properly in FIPS mode
- Resolves: rhbz#2083274
- openssl req defaults on PKCS#8 encryption changed to AES-256-CBC
- Resolves: rhbz#2063947

* Fri May 06 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-28
- OpenSSL should not accept custom elliptic curve parameters
- Resolves rhbz#2066412
- OpenSSL should not accept explicit curve parameters in FIPS mode
- Resolves rhbz#2058663

* Fri May 06 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-27
- Change FIPS module version to include hash of specfile, patches and sources
  Resolves: rhbz#2070550

* Thu May 05 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-26
- OpenSSL FIPS module should not build in non-approved algorithms
- Resolves: rhbz#2081378

* Mon May 02 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-25
- FIPS provider should block RSA encryption for key transport.
- Other RSA encryption options should still be available
- Resolves: rhbz#2053289

* Thu Apr 28 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-24
- Fix regression in evp_pkey_name2type caused by tr_TR locale fix
  Resolves: rhbz#2071631

* Wed Apr 20 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-23
- Fix openssl curl error with LANG=tr_TR.utf8
- Resolves: rhbz#2071631

* Mon Mar 28 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-22
- FIPS provider should block RSA encryption for key transport
- Resolves: rhbz#2053289

* Tue Mar 22 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-21
- Fix occasional internal error in TLS when DHE is used
- Resolves: rhbz#2004915

* Fri Mar 18 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-20
- Fix acceptance of SHA-1 certificates with rh-allow-sha1-signatures = yes when
  no OpenSSL library context is set
- Resolves: rhbz#2065400

* Fri Mar 18 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-19
- Fix TLS connections with SHA1 signatures if rh-allow-sha1-signatures = yes
- Resolves: rhbz#2065400

* Wed Mar 16 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-18
- CVE-2022-0778 fix
- Resolves: rhbz#2062315

* Thu Mar 10 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-17
- Fix invocation of EVP_PKEY_CTX_set_rsa_padding(RSA_PKCS1_PSS_PADDING) before
  setting an allowed digest with EVP_PKEY_CTX_set_signature_md()
- Skipping 3.0.1-16 due to version numbering confusion with the RHEL-9.0 branch
- Resolves: rhbz#2062640

* Tue Mar 01 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-15
- Allow SHA1 in SECLEVEL 2 if rh-allow-sha1-signatures = yes
- Resolves: rhbz#2060510

* Fri Feb 25 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-14
- Prevent use of SHA1 with ECDSA
- Resolves: rhbz#2031742

* Fri Feb 25 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-13
- OpenSSL will generate keys with prime192v1 curve if it is provided using explicit parameters
- Resolves: rhbz#1977867

* Thu Feb 24 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 1:3.0.1-12
- Support KBKDF (NIST SP800-108) with an R value of 8bits
- Resolves: rhbz#2027261

* Wed Feb 23 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-11
- Allow SHA1 usage in MGF1 for RSASSA-PSS signatures
- Resolves: rhbz#2031742

* Wed Feb 23 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-10
- rebuilt

* Tue Feb 22 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-9
- Allow SHA1 usage in HMAC in TLS
- Resolves: rhbz#2031742

* Tue Feb 22 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-8
- OpenSSL will generate keys with prime192v1 curve if it is provided using explicit parameters
- Resolves: rhbz#1977867
- pkcs12 export broken in FIPS mode
- Resolves: rhbz#2049265

* Tue Feb 22 2022 Clemens Lang <cllang@redhat.com> - 1:3.0.1-8
- Disable SHA1 signature creation and verification by default
- Set rh-allow-sha1-signatures = yes to re-enable
- Resolves: rhbz#2031742

* Thu Feb 03 2022 Sahana Prasad <sahana@redhat.com> - 1:3.0.1-7
- s_server: correctly handle 2^14 byte long records
- Resolves: rhbz#2042011

* Tue Feb 01 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-6
- Adjust FIPS provider version
- Related: rhbz#2026445

* Wed Jan 26 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-5
- On the s390x, zeroize all the copies of TLS premaster secret
- Related: rhbz#2040448

* Fri Jan 21 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-4
- rebuilt

* Fri Jan 21 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.1-3
- KATS tests should be executed before HMAC verification
- Restoring fips=yes for SHA1
- Related: rhbz#2026445, rhbz#2041994

* Thu Jan 20 2022 Sahana Prasad <sahana@redhat.com> - 1:3.0.1-2
- Add enable-buildtest-c++ to the configure options.
- Related: rhbz#1990814

* Tue Jan 18 2022 Sahana Prasad <sahana@redhat.com> - 1:3.0.1-1
- Rebase to upstream version 3.0.1
- Fixes CVE-2021-4044 Invalid handling of X509_verify_cert() internal errors in libssl
- Resolves: rhbz#2038910, rhbz#2035148

* Mon Jan 17 2022 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.0-7
- Remove algorithms we don't plan to certify from fips module
- Remove native fipsmodule.cnf
- Related: rhbz#2026445

* Tue Dec 21 2021 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.0-6
- openssl speed should run in FIPS mode
- Related: rhbz#1977318

* Wed Nov 24 2021 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.0-5
- rebuilt for spec cleanup
- Related: rhbz#1985362

* Thu Nov 18 2021 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.0-4
- Embed FIPS HMAC in fips.so
- Enforce loading FIPS provider when FIPS kernel flag is on
- Related: rhbz#1985362

* Thu Oct 07 2021 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.0-3
- Fix memory leak in s_client
- Related: rhbz#1996092

* Mon Sep 20 2021 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.0-2
- Avoid double-free on error seeding the RNG.
- KTLS and FIPS may interfere, so tests need to be tuned
- Resolves: rhbz#1952844, rhbz#1961643

* Thu Sep 09 2021 Sahana Prasad <sahana@redhat.com> - 1:3.0.0-1
- Rebase to upstream version 3.0.0
- Related: rhbz#1990814

* Wed Aug 25 2021 Sahana Prasad <sahana@redhat.com> - 1:3.0.0-0.beta2.7
- Removes the dual-abi build as it not required anymore. The mass rebuild
  was completed and all packages are rebuilt against Beta version.
- Resolves: rhbz#1984097

* Mon Aug 23 2021 Dmitry Belyavskiy <dbelyavs@redhat.com> - 1:3.0.0-0.beta2.6
- Correctly process CMS reading from /dev/stdin
- Resolves: rhbz#1986315

* Mon Aug 16 2021 Sahana Prasad <sahana@redhat.com> - 3.0.0-0.beta2.5
- Add instruction for loading legacy provider in openssl.cnf
- Resolves: rhbz#1975836

* Mon Aug 16 2021 Sahana Prasad <sahana@redhat.com> - 3.0.0-0.beta2.4
- Adds support for IDEA encryption.
- Resolves: rhbz#1990602

* Tue Aug 10 2021 Sahana Prasad <sahana@redhat.com> - 3.0.0-0.beta2.3
- Fixes core dump in openssl req -modulus
- Fixes 'openssl req' to not ask for password when non-encrypted private key
  is used
- cms: Do not try to check binary format on stdin and -rctform fix
- Resolves: rhbz#1988137, rhbz#1988468, rhbz#1988137

* Mon Aug 09 2021 Mohan Boddu <mboddu@redhat.com> - 1:3.0.0-0.beta2.2.1
- Rebuilt for IMA sigs, glibc 2.34, aarch64 flags
  Related: rhbz#1991688

* Wed Aug 04 2021 Dmitry Belyavskiy <dbelyavs@redhat.com> - 3.0.0-0.beta2.2
- When signature_algorithm extension is omitted, use more relevant alerts
- Resolves: rhbz#1965017

* Tue Aug 03 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.beta2.1
- Rebase to upstream version beta2
- Related: rhbz#1903209

* Thu Jul 22 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.beta1.5
- Prevents creation of duplicate cert entries in PKCS #12 files
- Resolves: rhbz#1978670

* Wed Jul 21 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.beta1.4
- NVR bump to update to OpenSSL 3.0 Beta1

* Mon Jul 19 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.beta1.3
- Update patch dual-abi.patch to add the #define macros in implementation
  files instead of public header files

* Wed Jul 14 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.beta1.2
- Removes unused patch dual-abi.patch

* Wed Jul 14 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.beta1.1
- Update to Beta1 version
- Includes a patch to support dual-ABI, as Beta1 brekas ABI with alpha16

* Tue Jul 06 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha16.7
- Fixes override of openssl_conf in openssl.cnf
- Use AI_ADDRCONFIG only when explicit host name is given
- Temporarily remove fipsmodule.cnf for arch i686
- Fixes segmentation fault in BN_lebin2bn
- Resolves: rhbz#1975847, rhbz#1976845, rhbz#1973477, rhbz#1975855

* Fri Jul 02 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha16.6
- Adds FIPS mode compatibility patch (sahana@redhat.com)
- Related: rhbz#1977318

* Fri Jul 02 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha16.5
- Fixes system hang issue when booted in FIPS mode (sahana@redhat.com)
- Temporarily disable downstream FIPS patches
- Related: rhbz#1977318

* Fri Jun 11 2021 Mohan Boddu <mboddu@redhat.com> 3.0.0-0.alpha16.4
- Speeding up building openssl (dbelyavs@redhat.com)
  Resolves: rhbz#1903209

* Fri Jun 04 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha16.3
- Fix reading SPKAC data from stdin
- Fix incorrect OSSL_PKEY_PARAM_MAX_SIZE for ed25519 and ed448
- Return 0 after cleanup in OPENSSL_init_crypto()
- Cleanup the peer point formats on regotiation
- Fix default digest to SHA256

* Thu May 27 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha16.2
- Enable FIPS via config options

* Mon May 17 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha16.1
- Update to alpha 16 version
  Resolves: rhbz#1952901 openssl sends alert after orderly connection close

* Mon Apr 26 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha15.1
- Update to alpha 15 version
  Resolves: rhbz#1903209, rhbz#1952598, 

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 1:3.0.0-0.alpha13.1.1
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Fri Apr 09 2021 Sahana Prasad <sahana@redhat.com> 3.0.0-0.alpha13.1
- Update to new major release OpenSSL 3.0.0 alpha 13
  Resolves: rhbz#1903209
